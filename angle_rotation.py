import cv2
import numpy as np
from scipy.ndimage import interpolation as inter
import random
import glob
import os

def correct_skew(image, delta=1, limit=5):
    def determine_score(arr, angle):
        data = inter.rotate(arr, angle, reshape=False, order=0)
        histogram = np.sum(data, axis=1)
        score = np.sum((histogram[1:] - histogram[:-1]) ** 2)
        return histogram, score

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

    scores = []
    angles = np.arange(-limit, limit + delta, delta)
    for angle in angles:
        histogram, score = determine_score(thresh, angle)
        scores.append(score)

    best_angle = angles[scores.index(max(scores))]

    (h, w) = image.shape[:2]
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, best_angle, 1.0)
    rotated = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC, \
              borderMode=cv2.BORDER_REPLICATE)

    return best_angle, rotated

def rotation_image(image_paths):
    img_paths = glob.glob(image_paths)
    for path in img_paths:
        name = os.path.basename(path)
        name = name.split('.')[0]

        image = cv2.imread(path)

        angle = random.randint(-3,3)

        (h, w) = image.shape[:2]
        center = (w // 2, h // 2)
        M = cv2.getRotationMatrix2D(center, angle, 1.0)
        rotated = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC, \
              borderMode=cv2.BORDER_REPLICATE)

        cv2.imwrite("rotation_iamge/"+str(name)+str(".jpg"),rotated)
        print("[INFOR] complete rotation ... ")



if __name__ == '__main__':
    # image = cv2.imread('img/2.jpg')
    # im_crop = image[0:700,0:500]
    # angle, rotated = correct_skew(im_crop)
    # print(angle)
    # cv2.imshow('rotated', rotated)
    # cv2.waitKey()

    rotation_image("VietinBank/*.jpg")


