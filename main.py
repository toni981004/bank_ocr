from socket import gaierror
import cv2
import numpy as np
import pandas as pd

from scipy.ndimage import interpolation as inter

from openpyxl import load_workbook

from openpyxl.styles import Alignment,Font,PatternFill,Border,Side

import pytesseract
import glob
import os

import numpy as np

from pdf2image import convert_from_path


def correct_skew(image, delta=1, limit=5):
    def determine_score(arr, angle):
        data = inter.rotate(arr, angle, reshape=False, order=0)
        histogram = np.sum(data, axis=1)
        score = np.sum((histogram[1:] - histogram[:-1]) ** 2)
        return histogram, score

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

    scores = []
    angles = np.arange(-limit, limit + delta, delta)
    for angle in angles:
        histogram, score = determine_score(thresh, angle)
        scores.append(score)

    best_angle = angles[scores.index(max(scores))]

    (h, w) = image.shape[:2]
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, best_angle, 1.0)
    rotated = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC,borderMode=cv2.BORDER_REPLICATE)

    return best_angle, rotated

def Histogram_Equalization(image_path):

    image = cv2.imread(image_path)

    #tính góc lệch
    angle,image_rotated = correct_skew(image)
    # print("Angle image : {}".format(angle))

    gray_image = cv2.cvtColor(image_rotated,cv2.COLOR_BGR2GRAY)

    gaussian = cv2.GaussianBlur(gray_image,(3,3),0)

    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    cl1 = clahe.apply(gaussian)

    return cl1

def sort_contours(cnts, method="left-to-right"):
    # initialize the reverse flag and sort index
    reverse = False
    i = 0
    # handle if we need to sort in reverse
    if method == "right-to-left" or method == "bottom-to-top":
        reverse = True
    # handle if we are sorting against the y-coordinate rather than
    # the x-coordinate of the bounding box
    if method == "top-to-bottom" or method == "bottom-to-top":
        i = 1
    # construct the list of bounding boxes and sort them from top to
    # bottom
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
    key=lambda b:b[1][i], reverse=reverse))
    # return the list of sorted contours and bounding boxes
    return (cnts, boundingBoxes)

def detect_line(image):
    #resize image
    h = 1300
    w = (1300*image.shape[1])//image.shape[0]
    img = cv2.resize(image,(w,h))

    #thresholding the image to a binary image
    thresh, img_bin = cv2.threshold(img, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

    #inverting the image
    img_bin = 255 - img_bin

    # Length(width) of kernel as 100th of total width
    kernel_len = np.array(img).shape[1]//130

    # Defining a vertical kernel to detect all vertical lines of image
    ver_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, kernel_len))

    # Defining a horizontal kernel to detect all horizontal lines of image
    hor_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_len, 1))

    # A kernel of 2x2
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))
    image_1 = cv2.erode(img_bin, ver_kernel, iterations=3)
    vertical_lines = cv2.dilate(image_1, ver_kernel, iterations=3)

    #Use horizontal kernel to detect and save the horizontal lines in a jpg
    image_2 = cv2.erode(img_bin, hor_kernel, iterations=3)
    horizontal_lines = cv2.dilate(image_2, hor_kernel, iterations=3)

    #Eroding and thesholding the image
    img_vh = cv2.addWeighted(vertical_lines, 0.5, horizontal_lines, 0.5, 0.0)

    #Eroding and thesholding the image
    img_vh = cv2.erode(~img_vh, kernel, iterations=2)
    thresh, img_vh = cv2.threshold(img_vh,128,255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

    bitxor = cv2.bitwise_xor(img,img_vh)
    bitnot = cv2.bitwise_not(bitxor)

    # cv2.imwrite("bitnot_.jpg",bitnot)
    #
    # cv2.imwrite("vertical_line.jpg",vertical_lines)
    # cv2.imwrite("horizontal_line.jpg",horizontal_lines)
    # cv2.imwrite("img_vh.jpg",img_vh)

    return img_vh,img,bitnot

def find_box(img_vh,img,name):
    #find contours
    contours, hierarchy = cv2.findContours(img_vh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2:]

    # Sort all the contours by top to bottom.
    contours, boundingBoxes = sort_contours(contours, method="top-to-bottom")
    heights = [boundingBoxes[i][3] for i in range(len(boundingBoxes))]

    # Get mean of heights
    mean = np.mean(heights)
    # print("mean: {}".format(mean))

    # Create list box to store all boxes in
    box = []

    # Get position (x,y), width and height for every contour and show the contour on image
    for c in contours:
        #calculate box area
        area = cv2.contourArea(c)
        #box
        x, y, w, h = cv2.boundingRect(c)
        if (w < 1000 and h < 500) and area > 400:
            image = cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
            box.append([x, y, w, h])

    # cv2.imwrite("draw_box_{}.jpg".format(name),image)

    return box,mean

def find_column_row(list_box,mean):
    row = []
    column = []
    j = 0

    # Sorting the boxes to their respective row and column
    for i in range(len(list_box)):
        if (i == 0):
            column.append(list_box[i])
            previous = list_box[i]
        else:
            if (list_box[i][1] <= previous[1] + mean / 2):
                column.append(list_box[i])
                previous = list_box[i]
                if (i == len(list_box) - 1):
                    row.append(column)
            else:
                row.append(column)
                column = []
                previous = list_box[i]
                column.append(list_box[i])

     # calculating maximum number of cells
    countcol = 0
    for i in range(len(row)):
        countcol = len(row[i])
        if countcol > countcol:
            countcol = countcol

    # Retrieving the center of each column
    center = [int(row[i][j][0] + row[i][j][2] / 2) for j in range(len(row[i])) if row[0]]

    center = np.array(center)
    center.sort()
    # print("center = {} ".format(center))
    # Regarding the distance to the columns center, the boxes are arranged in respective order
    finalboxes = []
    for i in range(len(row)):
        lis = []
        for k in range(countcol):
            lis.append([])
        for j in range(len(row[i])):
            diff = abs(center - (row[i][j][0] + row[i][j][2] / 4))
            minimum = min(diff)
            indexing = list(diff).index(minimum)
            lis[indexing].append(row[i][j])
        finalboxes.append(lis)

    # print("column = {}".format(len(column)))
    # print("row = {}".format(len(row)))
    # print("count_col = {}".format(countcol))

    return finalboxes,column,row,countcol

def extract_text(finalboxes,image):

    def is_number(s):# Ham kiem tra ki tu so
        try:
            float(s) # for int, long and float
        except ValueError:
            try:
                complex(s) # for complex
            except ValueError:
                return False
        return True

    #============================= Hau xu ly chuoi cho cot 1 ================================
    def string_processing_column1(input_string):
        def structure(string):# ham kiem tra cau truc cua chuoi
            string_processed = ""
            array = list(string)
            for i in range(len(array)):
                if array[i] == '/' or is_number(array[i]) == True:
                    if array[i] == '/' and i == 0 :
                        array[i]=''
                    if array[i] == '/' and i == len(array) - 1:
                        array[i]=''
                    else:
                        string_processed += array[i]
            return string_processed

        string = structure(input_string)

        return string

    #============================= Hau xu ly chuoi cho cot 2 ================================
    def string_processing_column2(input_string):
        def structure(string):# ham kiem tra cau truc cua chuoi
            string_processed = ""
            array = list(string)
            for i in range(len(array)):
                if array[i] == ':' or is_number(array[i]) == True:
                    if array[i] == ':' and i == 0 :
                        array[i]=''
                    if array[i] == ':' and i == len(array) - 1:
                        array[i]=''
                    else:
                        string_processed += array[i]
            return string_processed

        string = structure(input_string)
        return string

    #============================= Hau xu ly chuoi cho cot 3 ================================
    def string_processing_column3(input_string):

        string = input_string.replace('_'|'|',' ')
        return string

    #============================= Hau xu ly chuoi cho cot 4 ================================
    def string_processing_column4(input_string):
        def structure(string):# ham kiem tra cau truc cua chuoi
            string_processed = ""
            array = list(string)
            for i in range(len(array)):
                if array[i] == ',' or is_number(array[i]) == True:
                    if array[i] == ',' and i == 0 :
                        array[i]=''
                    if array[i] == ',' and i == len(array) - 1:
                        array[i]=''
                    else:
                        string_processed += array[i]
            return string_processed

        string = structure(input_string)
        return string

    #============================= Hau xu ly chuoi cho cot 5 ================================
    def string_processing_column5(input_string):
        def structure(string):# ham kiem tra cau truc cua chuoi
            string_processed = ""
            array = list(string)
            for i in range(len(array)):
                if array[i] == ',' or is_number(array[i]) == True:
                    if array[i] == ',' and i == 0 :
                        array[i]=''
                    if array[i] == ',' and i == len(array) - 1:
                        array[i]=''
                    else:
                        string_processed += array[i]
            return string_processed

        string = structure(input_string)
        return string

    #============================= Hau xu ly chuoi cho cot 6 ================================
    def string_processing_column6(input_string):
        def structure(string):# ham kiem tra cau truc cua chuoi
            string_processed = ""
            array = list(string)
            for i in range(len(array)):
                if array[i] == ',' or is_number(array[i]) == True:
                    if array[i] == ',' and i == 0 :
                        array[i]=''
                    if array[i] == ',' and i == len(array) - 1:
                        array[i]=''
                    else:
                        string_processed += array[i]
            return string_processed

        string = structure(input_string)
        return string


    # from every single image-based cell/box the strings are extracted via pytesseract and stored in a list
    outer = []

    for i in range(len(finalboxes)):
        for j in range(len(finalboxes[i])):
            inner = ''
            if (len(finalboxes[i][j]) == 0):
                outer.append(' ')
            else:
                for k in range(len(finalboxes[i][j])):
                    y, x, w, h = finalboxes[i][j][k][0], finalboxes[i][j][k][1], finalboxes[i][j][k][2], \
                                 finalboxes[i][j][k][3]

                    finalimg = image[x:x + h, y:y + w]

                    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 1))

                    border = cv2.copyMakeBorder(finalimg, 2, 2, 2, 2, cv2.BORDER_CONSTANT, value=[255, 255])

                    resizing = cv2.resize(border, None, fx=2, fy=2, interpolation=cv2.INTER_CUBIC)

                    dilation = cv2.dilate(resizing, kernel, iterations=1)

                    erosion = cv2.erode(dilation, kernel, iterations=2)

                    out = pytesseract.image_to_string(erosion,lang='eng')

                    if i >= 2:
                        if j == 0:
                            out = string_processing_column1(out)
                        if j == 1:
                            out = string_processing_column2(out)
                        if j == 2:
                            pass
                        if j == 3:
                            out = string_processing_column4(out)
                        if j == 4:
                            out = string_processing_column5(out)
                        if j == 5:
                            out = string_processing_column6(out)

                    inner = inner + " " + out

                outer.append(inner)

    # Creating a dataframe of the generated OCR list
    arr = np.array(outer)

    dataframe = pd.DataFrame(arr.reshape(len(row), countcol))

    return dataframe

def Init_Table(book,sheet_name,path,count_row,count_column):
        sheet = book.get_sheet_by_name(sheet_name)
        count_row = count_row + 2
        count_column = count_column + 2
        # chieu cao dong dau
        sheet.row_dimensions[2].height = 50
        # chieu cao cua tung dong
        for i in range(count_row):
            if i >= 3:
                sheet.row_dimensions[i].height = 30
        # chieu rong cua tung cot
        sheet.column_dimensions['B'].width = 15
        sheet.column_dimensions['C'].width = 15
        sheet.column_dimensions['D'].width = 60
        sheet.column_dimensions['E'].width = 20
        sheet.column_dimensions['F'].width = 20
        sheet.column_dimensions['G'].width = 20
        #ke duong vien cot
        for i in range(count_row):
            if i >= 1:
                sheet.cell(row = i, column = 1).border = Border(left=Side(border_style='double',color='000000'),right=Side(border_style='double',color='000000'))
                sheet.cell(row = i, column = 1).alignment = Alignment(horizontal='center', vertical='center', wrap_text=True)
                sheet.cell(row = i, column = 1).font = Font(size = 14, name = 'Times New Roman')
                # sheet.cell(row = i, column = 1).fill = PatternFill(start_color="BFC9CA", fill_type = "solid")

        for i in range(count_row):
            if i >= 1:
                sheet.cell(row = i, column = 2).border = Border(right=Side(border_style='double',color='000000'))
                sheet.cell(row = i, column = 2).alignment = Alignment(horizontal='center', vertical='center', wrap_text=True)
                sheet.cell(row = i, column = 2).font = Font(size = 14, name = 'Times New Roman')
                # sheet.cell(row = i, column = 2).fill = PatternFill(start_color="BFC9CA", fill_type = "solid")

        for i in range(count_row):
            if i >= 1:
                sheet.cell(row = i, column = 3).border = Border(right=Side(border_style='double',color='000000'))
                sheet.cell(row = i, column = 3).alignment = Alignment(horizontal='center', vertical='center', wrap_text=True)
                sheet.cell(row = i, column = 3).font = Font(size = 14, name = 'Times New Roman')
                # sheet.cell(row = i, column = 3).fill = PatternFill(start_color="BFC9CA", fill_type = "solid")

        for i in range(count_row):
            if i >= 1:
                sheet.cell(row = i, column = 4).border = Border(right=Side(border_style='double',color='000000'))
                sheet.cell(row = i, column = 4).alignment = Alignment(horizontal='center', vertical='center', wrap_text=True)
                sheet.cell(row = i, column = 4).font = Font(size = 14, name = 'Times New Roman')
                # sheet.cell(row = i, column = 4).fill = PatternFill(start_color="BFC9CA", fill_type = "solid")

        for i in range(count_row):
            if i >= 1:
                sheet.cell(row = i, column = 5).border = Border(right=Side(border_style='double',color='000000'))
                sheet.cell(row = i, column = 5).alignment = Alignment(horizontal='center', vertical='center', wrap_text=True)
                sheet.cell(row = i, column = 5).font = Font(size = 14, name = 'Times New Roman')
                # sheet.cell(row = i, column = 5).fill = PatternFill(start_color="BFC9CA", fill_type = "solid")

        for i in range(count_row):
            if i >= 1:
                sheet.cell(row = i, column = 6).border = Border(right=Side(border_style='double',color='000000'))
                sheet.cell(row = i, column = 6).alignment = Alignment(horizontal='center', vertical='center', wrap_text=True)
                sheet.cell(row = i, column = 6).font = Font(size = 14, name = 'Times New Roman')
                # sheet.cell(row = i, column = 6).fill = PatternFill(start_color="BFC9CA", fill_type = "solid")

        for i in range(count_row):
            if i >= 1:
                sheet.cell(row = i, column = 7).border = Border(right=Side(border_style='double',color='000000'))
                sheet.cell(row = i, column = 7).alignment = Alignment(horizontal='center', vertical='center', wrap_text=True)
                sheet.cell(row = i, column = 7).font = Font(size = 14, name = 'Times New Roman')
                # sheet.cell(row = i, column = 7).fill = PatternFill(start_color="BFC9CA", fill_type = "solid")

        #duong ke ngang

        for i in range(count_column):
            if i >= 1:
                sheet.cell(row=count_row,column = i).border = Border(top=Side(border_style='double',color='000000'))

        if count_column == 6:
            for i in range(count_column):
                if i >= 1:
                    sheet.cell(row=2,column = i).border = Border(top=Side(border_style='double',color='000000'),
                                                                 bottom=Side(border_style='double',color='000000'),
                                                                 left=Side(border_style='double',color='000000'),
                                                                 right=Side(border_style='double',color='000000'))

                    sheet.cell(row=2,column = i).alignment = Alignment(horizontal='center', vertical='center', wrap_text=True)
                    sheet.cell(row=2,column = i).font = Font(size = 14, name = 'Times New Roman')
                    sheet.cell(row=2,column = i).fill = PatternFill(start_color="BFC9CA", fill_type = "solid")

        sheet.cell(row=2,column = 2).value = "Ngay ( Date )"
        sheet.cell(row=2,column = 3).value = "Gio ( Time )"
        sheet.cell(row=2,column = 4).value = "Noi dung giao dich ( Transaction description )"
        sheet.cell(row=2,column = 5).value = "No ( Debit )"
        sheet.cell(row=2,column = 6).value = "Co ( Credit )"
        sheet.cell(row=2,column = 7).value = "So Du ( Balance )"

        sheet.cell(row=3,column = 2).value = ""
        sheet.cell(row=3,column = 3).value = ""
        sheet.cell(row=3,column = 4).value = ""
        sheet.cell(row=3,column = 5).value = ""
        sheet.cell(row=3,column = 6).value = ""
        sheet.cell(row=3,column = 7).value = ""

        book.save(path)

def save_to_excel(dataframe,sheet_name,path,count_row,count_column):
    path = path
    book = load_workbook(path)
    writer = pd.ExcelWriter(path, engine='openpyxl')
    writer.book = book
    writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
    #save data to excel
    dataframe.to_excel(writer, sheet_name)
    writer.save()
    #init table
    if count_column == 6:
        Init_Table(book,sheet_name,path,count_row,count_column)

    book.save(path)
    writer.close()

def convert_pdf2img(path):

    pdf_dir = path

    os.chdir(pdf_dir)

    for pdf_file in os.listdir(pdf_dir):

        if pdf_file.endswith(".pdf"):

            pages = convert_from_path(pdf_file, 300)
            pdf_file = pdf_file[:-4]

            for page in pages:

                page.save("%s-page%d.jpg" % (pdf_file,pages.index(page)), "JPEG")





if __name__ == '__main__':

    convert_pdf2img("/home/toni/Documents/Banking/demo/VietinBank")

    img_paths = glob.glob("/home/toni/Documents/Banking/demo/VietinBank/*.jpg")

    for path in img_paths:

        name = os.path.basename(path)

        name = name.split('.')[0]

        print("================={}==================".format(name))
        print("{}/{}".format(img_paths.index(path),len(img_paths)))

        image = Histogram_Equalization(path)

        img_vh,image_resized,bitnot = detect_line(image)

        list_box ,mean = find_box(img_vh,image_resized,name)

        finalboxes,column,row,countcol = find_column_row(list_box,mean)

        dataframe = extract_text(finalboxes,bitnot)

        save_to_excel(dataframe,name,"/home/toni/Documents/Banking/demo/output_table4.xlsx",len(row),countcol)
